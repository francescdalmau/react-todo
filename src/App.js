import React from 'react';
import Footer from './components/Footer';
import AddTodo from './containers/AddTodo';
import VisibleTodoList from './containers/VisibleTodoList';
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';

const App = () => (
  <div>
    <div className='title'>
      todos
    </div>
    <Paper className='main-container' elevation={4}>
      <AddTodo />
      <VisibleTodoList />
      <Divider />
      <Footer />
    </Paper>
  </div>
);

export default App;
