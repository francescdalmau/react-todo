const todos = (state = [], action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false
        }
      ];
    case 'DELETE_TODO':
      return state.filter(todo => todo.id !== action.id);
    case 'TOGGLE_TODO':
      return state.map(todo =>
        (todo.id === action.id)
          ? {...todo, completed: !todo.completed}
          : todo
      );
    case 'LOAD_TODOS':
      return action.todos;
    case 'CLEAR_COMPLETED':
      return state.filter(todo => !todo.completed);
    case 'EDIT_TODO':
      return state.map(todo =>
        (todo.id === action.id)
          ? {...todo, text: action.text}
          : todo
      );
    default:
      return state;
  }
};

export default todos;
