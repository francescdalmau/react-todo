import backend from '../backend';

function generateKey () {
  return Date.now();
}

export const loadTodos = () => {
  return {
    type: 'LOAD_TODOS',
    todos: backend.getTodos()
  };
};

export const addTodo = (text, todos) => {
  const id = generateKey();

  backend.saveNewTodo(id, text);

  return {
    type: 'ADD_TODO',
    id,
    text
  };
};

export const setVisibilityFilter = filter => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  };
};

export const toggleTodo = id => {
  backend.toggleTodo(id);
  return {
    type: 'TOGGLE_TODO',
    id
  };
};

export const editTodo = (id, text) => {
  backend.editTodo(id, text);
  return {
    type: 'EDIT_TODO',
    id,
    text
  };
}

export const deleteTodo = id => {
  backend.deleteTodo(id);
  return {
    type: 'DELETE_TODO',
    id
  };
};

export const clearCompleted = () => {
  backend.clearCompleted();
  return {
    type: 'CLEAR_COMPLETED'
  };
};

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
};
