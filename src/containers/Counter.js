import React from 'react';
import { connect } from 'react-redux';

const Counter = ({pending = 0}) => (
  <div className='counter'>Pending: {pending}</div>
);

const mapStateToProps = (state, ownProps) => {
  return {
    pending: state.todos.filter(todo => !todo.completed).length
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter);
