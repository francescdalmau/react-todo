import React from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../actions';
import TextField from 'material-ui/TextField';

const AddTodo = ({ dispatch }) => {
  const onKeyPress = (ev) => {
    if (ev.charCode === 13 && ev.target.value.trim().length) {
      dispatch(addTodo(ev.target.value));
      ev.target.value = '';
    }
  };

  return (
    <div className='input-zone'>
      <TextField
        className='input-zone__text-field'
        autoFocus
        fullWidth
        placeholder='What needs to be done?'
        onKeyPress={onKeyPress}
      />
    </div>
  );
};

export default connect()(AddTodo);
