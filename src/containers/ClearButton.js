import { connect } from 'react-redux';
import { clearCompleted } from '../actions';
import Link from '../components/Link';

const mapStateToProps = (state, ownProps) => {
  return {
    active: false
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(clearCompleted());
    }
  };
};

const ClearButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(Link);

export default ClearButton;
