const saveNewTodo = (id, text) => {
  const todos = [
    ...getTodos(),
    {
      id,
      text,
      completed: false
    }
  ];

  saveTodos(todos);
};

const toggleTodo = (id) => {
  const todos = getTodos().map(todo =>
    (todo.id === id)
      ? {...todo, completed: !todo.completed}
      : todo
  );

  saveTodos(todos);
};

const getTodos = () => {
  return JSON.parse(window.localStorage.todos || '[]');
};

const saveTodos = (todos) => {
  window.localStorage.setItem('todos', JSON.stringify(todos));
};

const deleteTodo = (id) => {
  const todos = getTodos().filter(todo => todo.id !== id);
  saveTodos(todos);
};

const clearCompleted = () => {
  const todos = getTodos().filter(todo => !todo.completed);
  saveTodos(todos);
};

const editTodo = (id, text) => {
  const todos = getTodos().map(todo =>
    (todo.id === id)
      ? {...todo, text}
      : todo
  );
  saveTodos(todos);
}

export default {
  saveNewTodo,
  toggleTodo,
  getTodos,
  deleteTodo,
  clearCompleted,
  editTodo
};
