import React from 'react';
import FilterLink from '../containers/FilterLink';
import ClearButton from '../containers/ClearButton';
import Counter from '../containers/Counter';
import { VisibilityFilters } from '../actions';

const Footer = () => (
  <div className='footer'>
    <Counter />
    <div className='filter'>
      <FilterLink filter={VisibilityFilters.SHOW_ALL}>
        ALL
      </FilterLink>
      <FilterLink filter={VisibilityFilters.SHOW_ACTIVE}>
        ACTIVE
      </FilterLink>
      <FilterLink filter={VisibilityFilters.SHOW_COMPLETED}>
        COMPLETED
      </FilterLink>
    </div>
    <div className='clear'>
      <ClearButton>
        CLEAR COMPLETED
      </ClearButton>
    </div>
  </div>
);

export default Footer;
