import React from 'react';
import PropTypes from 'prop-types';
import Todo from './Todo';
import List from 'material-ui/List';

const TodoList = ({ todos, onTodoClick, onTodoDelete, onTodoEdit }) => (
  <List>
    {
      todos.length
        ? todos.map(todo => (
          <Todo
            key={todo.id}
            {...todo}
            onClick={() => {
              onTodoClick(todo.id);
            }}
            onDelete={() => {
              onTodoDelete(todo.id);
            }}
            onEdit={(text) => {
              onTodoEdit(todo.id, text);
            }}
          />
        ))
        : <div className='todo__empty-msg'>Nothing to see here.</div>
    }
  </List>
);

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onTodoClick: PropTypes.func.isRequired,
  onTodoDelete: PropTypes.func.isRequired,
  onTodoEdit: PropTypes.func.isRequired
};

export default TodoList;
