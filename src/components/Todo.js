import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';

import DeleteIcon from 'material-ui-icons/Delete';
import Create from 'material-ui-icons/Create';
import Clear from 'material-ui-icons/Clear';

const styles = {
  checked: {
    color: '#6ec190' // Success color
  }
};

class Todo extends Component {
  state = {
    editing : false
  }

  startEditing = () => {
    this.setState({editing: true});
  }

  cancelEditing = () => {
    this.setState({editing: false});
  }

  onKeyPress = (ev) => {
    if (ev.charCode === 13) {
      if (ev.target.value.trim().length) {
        this.props.onEdit(ev.target.value);
        this.setState({editing: false})
      } else {
        this.cancelEditing()
      }

    }
  }

  render () {
    const {onClick, completed, text, onDelete, classes} = this.props;
    return (
      <ListItem
        dense
        className='todo__item'
      >
        <Checkbox
          checked={completed}
          tabIndex={-1}
          onClick={onClick}
          classes={{
            checked: classes.checked
          }}
        />
        {this.state.editing
        ? <TextField
          autoFocus
          onBlur={this.cancelEditing}
          fullWidth
          defaultValue={text}
          onKeyPress={this.onKeyPress}
        />
        : <ListItemText primary={text} />}
        {
          !this.state.editing
          ? <ListItemSecondaryAction>
            <div>
              <IconButton onClick={this.startEditing}>
                <Create className='edit-icon' />
              </IconButton>
              <IconButton onClick={onDelete}>
                <DeleteIcon className='delete-icon' />
              </IconButton>
            </div>
          </ListItemSecondaryAction>
          : <ListItemSecondaryAction>
              <IconButton onClick={this.cancelEditing}>
                <Clear className='delete-icon' />
              </IconButton>
          </ListItemSecondaryAction>
      }
      </ListItem>
    )
  }
}

Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit:  PropTypes.func.isRequired
};

export default withStyles(styles)(Todo);
