# React-Redux-SASS ToDo app

This is  a ToDo list exercise done with React-Redux and SASS.

## Available Scripts
In the project directory, you can run:

### `yarn start`
Runs the app. The app compiled code is located in the **public** folder.
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `yarn watch`
Runs a whatch process to update the files for  **yarn start**.
